describe('Calc', function() {

beforeEach(function()
{
    browser.get("http://www.way2automation.com/angularjs-protractor/calc/");
    element(by.model('first')).sendKeys("4");
    element(by.model('second')).sendKeys("4");
    browser.sleep(3000);
}
);
afterEach(function()
{
    console.log("Done");
}
);
it('Add', function() {

    element(by.model("operator")).element(by.css("[value='ADDITION']")).click();
    element(by.buttonText("Go!")).click();
    browser.sleep(3000);
    element(by.className('ng-binding')).getText().then(function(Result)

{
    console.log("Result of 4+4="+Result);
    expect(parseInt(Result)).toBe(8);
}
 

);   
 
  });

it('Div', function() {

    element(by.model("operator")).element(by.css("[value='DIVISION']")).click();
    element(by.buttonText("Go!")).click();
    browser.sleep(3000);
    element(by.className('ng-binding')).getText().then(function(Result)

{
    console.log("Result of 4-4="+Result);
    expect(parseInt(Result)).not.toBe(8);
}

);
browser.sleep(3000);
  });
});